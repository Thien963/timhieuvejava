package Chapter2OperatorsAndStatemnets;

public class NumericPromotion {
	public static void main(String[] args) {
		/*int x =1;
		long y = 33;
		System.out.println(x * y);*/
		
		double a = 31.21;
		float b = 3.1f;
		System.out.println(a + b);
		
		short c  = 10;
		short d = 3;
		System.out.println(c / d);
		
		short x = 14;
		float y = 13;
		double z = 30;
		System.out.println(x * y / z);
		
		
	}
}
