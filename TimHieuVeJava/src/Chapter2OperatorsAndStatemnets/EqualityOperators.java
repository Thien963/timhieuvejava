package Chapter2OperatorsAndStatemnets;

import java.io.File;

public class EqualityOperators {
	public static void main(String[] args) {
		/*
		 * boolean x = true == 3; // KHÔNG TẠO 
		 * boolean y = false! = "Giraffe"; // KHÔNGTẠO 
		 * boolean z = 3 == "Kangaroo"; // KHÔNG TẠO 
		 * System.out.println(x);
		 * System.out.println(y); 
		 * System.out.println(z);
		 */
		
		boolean y = false;
		boolean x = (y = true);
		System.out.println (x); // Kết quả đúng

		File x1 = new File("myFile.txt");
		File y1 = new File("myFile.txt");
		File z = x1;
		System.out.println (x1 == y1); // Kết quả sai
		System.out.println (x1 == z); // Kết quả đúng

	}
}
