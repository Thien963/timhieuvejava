package Chapter2OperatorsAndStatemnets;

import java.util.Arrays;
public class LogicalComplementAndNegation {
	public static void main(String[] args) {
		boolean x = false;
		System.out.println(x); // false
		x = !x;
		System.out.println(x); // true

		
		double y = 1.21;
		System.out.println(y); // 1.21
		y = -y;
		System.out.println(y); // -1.21
		y = -y;
		System.out.println(y); // 1.21

		
		/*
		 * int a = !5; // KHÔNG TẠO 
		 * boolean b = -true; // KHÔNG TẠO 
		 * boolean c = !0; // KHÔNG TẠO
		 */
	}
}
