package Chapter2OperatorsAndStatemnets;

import java.util.Scanner;

public class NameProperty{
	static Scanner input = new Scanner(System.in);
	public static void main (String [] args){
		String s = input.nextLine().trim();
		int count = 0;
		String a = s.replaceAll("\\s+", " ");
		String []temp = a.split(" ");
		String b ="";
		for(int i = 0; i < temp.length; i++){
			temp[i] = String.valueOf(temp[i]).toLowerCase();
			
		}
		for(int i = 0; i < temp.length; i++){
			b = b + String.valueOf(temp[i].charAt(0)).toUpperCase() + temp[i].substring(1);
			if(i < temp.length - 1){
				b += " ";
			}
		}
		System.out.println(b);
	}
}