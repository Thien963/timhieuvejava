package Chapter2OperatorsAndStatements;

public class ArithmeticOprators {
	public static void main(String[] args) {
		int x = 2 * 5 + 3 * 4 - 8;
		int y = 2 * ((5 + 3) * 4 -8);
		System.out.println(x);
		System.out.println(y);
		
		System.out.println(9 / 3); //3
		System.out.println(9 % 3); //0
		
		System.out.println(10 / 3); //3
		System.out.println(10 % 3); //1
		
		System.out.println(11 / 3); //3
		System.out.println(11 % 3); //2
		
		System.out.println(12 / 3); //4
		System.out.println(12 % 3); //0
		
	}
}
