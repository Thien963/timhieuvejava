package Chapter2OperatorsAndStatements;

public class CastingPrimitiveValues {
	public static void main(String[] args) {
		int x = (int) 1.0;
		short y = (short) 1921222; // Được lưu trữ vào năm 20678
		int z = (int) 9l;
		long t = 1923011398193810323L;
		
		System.out.println(2147483647 + 1); //-2147483648
		
		/*
		 * short a = 10; 
		 * short b = 3; 
		 * short c; //
		 * c= a * b; // KHÔNG TẠO
		 * System.out.println(c);
		 */
		
		short a = 10;
		short b = 3;
		short c = (short) (a * b);
		System.out.println(c);

	}
}
