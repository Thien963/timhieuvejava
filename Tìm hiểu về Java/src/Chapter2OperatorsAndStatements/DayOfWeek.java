package Chapter2OperatorsAndStatements;

import java.util.Scanner;

public class DayOfWeek{
	private static Scanner sc;

	public static void main(String [] args){
		sc = new Scanner(System.in);
		//System.out.println("Nhap d: ");
		int m = sc.nextInt();
		//System.out.println("Nhap m: ");
		int d = sc.nextInt();
		//System.out.println("Nhap y: ");
		int y = sc.nextInt();
		if((1 <= m && m <= 12) && (1 <= d && d <= 31)){
			int y0 = y - (14 - m)/12;
			int x = y0 + y0/4 - y0/100 + y0/400;
			int m0 = m + 12 *((14 - m)/12) - 2;
			int d0 = (d + x + (31*m0) / 12) % 7;
			System.out.print(d0);
		}

	}
}
