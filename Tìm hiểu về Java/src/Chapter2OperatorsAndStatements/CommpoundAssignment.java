package Chapter2OperatorsAndStatements;

public class CommpoundAssignment {
	public static void main(String[] args) {
		int x = 2, z = 3;
		x = x * z; // Toán tử gán đơn giản
		x *= z; // Toán tử gán hợp chất
		
		/*
		 * long x = 10; 
		 * int y = 5; 
		 * y = y * x; // KHÔNG TẠO
		 */

		long a= 5;
		long b = (a = 3);
		System.out.println (a); // Kết quả 3
		System.out.println (b); // Ngoài ra, đầu ra 3

		
		
	}
}
