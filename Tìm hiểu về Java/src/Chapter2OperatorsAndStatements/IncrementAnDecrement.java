package Chapter2OperatorsAndStatements;

public class IncrementAnDecrement {
	public static void main(String[] args) {
		int counter = 0;
		System.out.println (counter); // Kết quả 0
		System.out.println (counter ++); // Kết quả 1
		System.out.println (counter); // Kết quả 1
		System.out.println (counter--); // Kết quả 1
		System.out.println (counter); // Kết quả 0

		int x = 3;
		int y = ++ x * 5 / x-- + --x;
		System.out.println ("x là" + x);
		System.out.println ("y là" + y);

		int a = 4 * 5 / x-- + --x; // x giá trị được gán của 4
		System.out.println(a);
		int b = 4 * 5/4 + --x; // x giá trị được gán của 3
		System.out.println(b);
		int c = 4 * 5/4 + 2; // x giá trị được gán của 2
		System.out.println(c);
	}
}
