package Chapter2OperatorsAndStatements;

import java.util.Scanner;

public class AddMatrix {
		static Scanner sc = new Scanner(System.in);
		public static void input(int [][]a){
			for(int i = 0; i < a.length; i++){
				for(int j = 0; j < a[0].length; j++){
					a[i][j] = sc.nextInt();
				}
			}
		}
		public static void output(int [][]a){
			for(int i = 0; i < a.length; i++){
				for(int j = 0; j < a[0].length; j++){
					System.out.print(a[i][j]+" ");
				}
				System.out.println();
			}
		}
		public static int[][] sumArray(int [][]a, int [][]b){
			int c[][] = new int[a.length][a[0].length];
			for(int i = 0; i < a.length; i++){
				for(int j = 0; j < a[0].length; j++){
					c[i][j] = 2 * a[i][j] + 3 * b[i][j];
				}
			}
			return c;
		}
		public static void main (String []args){
			int M = sc.nextInt();
			int N = sc.nextInt();
			int a[][] = new int[M][N];
			int b[][] = new int[M][N];
			int c[][] = new int[M][N];
			input(a);
			input(b);
			c = sumArray(a,b);
			output(c);
			
		}
	}


